<?php
/*
Plugin Name: My Google Maps
Description: plugin that make google maps for stores.
Version: 1.0
Author: Vitaliy
*/
/**
 *check WordPress version
 */
register_activation_hook( __FILE__, 'my_google_maps_activate' );
function my_google_maps_activate() {
	global $wp_version;

	if ( version_compare( '4.7.5', $wp_version, '>' ) ) {
		wp_die( 'this plugin cannot activated because you use old version wordpress, update your wordpress please!' . $wp_version );
	}
}

/**
 * add new taxonomy
 */
add_action( 'init', 'my_store_taxonomy' );
function my_store_taxonomy() {
	$labels = array(
		'name'              => __( 'Shops category' ),
		'singular_name'     => __( 'Shop category' ),
		'search_items'      => __( 'Search category shop' ),
		'all_items'         => __( 'All' ),
		'parent_item'       => __( 'Parent' ),
		'parent_item_colon' => __( 'Pereht category:' ),
		'edit_item'         => __( 'Edit shop category' ),
		'view item'         => __( 'View shop category' ),
		'update_item'       => __( 'Update shop category' ),
		'add_new_item'      => __( 'Add shop category' ),
		'new_item_name'     => __( 'New category name' ),
		'menu_name'         => __( 'Category shops' ),
	);
	$args   = array(
		'label'             => __( 'Shops category' ),
		'labels'            => $labels,
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => ''
		),
	);
	register_taxonomy( 'taxstore', array( 'mystores' ), $args );

}

/**
 * add custom post
 */
add_action( 'init', 'register_my_post_stores' );
function register_my_post_stores() {
	$args = array(
		'label'             => __( 'My Shops' ),
		'labels'            => array(
			'name'               => __( 'My Shops' ),
			'singular_name'      => __( 'My Shop' ),
			'add_new'            => __( 'Add My Shop' ),
			'add_new_item'       => __( 'Add My Shop Item' ),
			'edit_item'          => __( 'Edit My Shop' ),
			'new_item'           => __( 'New My Shop' ),
			'view_item'          => __( 'View My Shop' ),
			'search_items'       => __( 'Search My Shop' ),
			'not_found'          => __( 'Not Found' ),
			'not_found_in_trash' => __( 'Not Found In Trash' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
		'taxonomies'        => array( 'taxstore' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	);
	register_post_type( 'mystores', $args );

}

/**
 * add metaboxes(custom for mystores)
 */
add_action( 'add_meta_boxes', 'my_store_boxes' );
function my_store_boxes() {
	add_meta_box(
		'mystores',
		__( 'Store information' ),
		'my_store_content',
		'mystores',
		'side',
		'high'
	);
}

function my_store_content( $post ) {
	$name    = get_post_meta( $post->ID, '_name', true );
	$desc    = get_post_meta( $post->ID, '_desc', true );
	$address = get_post_meta( $post->ID, '_address', true );
	//wp_nonce_field( 'my_action_events', 'my_events' );

	echo "<p><label for='meta-name' />";
	_e( 'Store name:' );
	echo "</label>";
	echo "<input id='meta-name' class='widefat' name='my-google-maps-name' type='text' value='";
	echo esc_attr( $name );
	echo "'/></p>";

	echo "<p><label for='meta-desc' />";
	_e( 'Store description:' );
	echo "</label>";
	echo "<textarea id='meta-desc' class='widefat' name='my-google-maps-desc' required>";
	echo esc_attr( $desc );
	echo "</textarea></p>";

	echo "<p><label for='meta-addres' />";
	_e( 'Store address:' );
	echo "</label>";
	echo "<input id='meta-address' class='widefat' name='my-google-maps-address' type='text' value='";
	echo esc_attr( $address );
	echo "'/></p>";
}

/**
 * save metaboxes values
 */
add_action( 'save_post', 'myshop_save_meta_box' );
function myshop_save_meta_box( $post_id ) {
	$name    = trim( strip_tags( $_POST['my-google-maps-name'] ) );
	$desc    = trim( strip_tags( $_POST['my-google-maps-desc'] ) );
	$address = trim( strip_tags( $_POST['my-google-maps-address'] ) );

	if ( ! isset( $name ) && ! isset( $desc ) && ! isset( $address ) ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $name ) && isset( $desc ) && isset( $address ) ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$user_api_key = 'AIzaSyD7PyCZw-qysy7M6O4GNmoY_ASSdRhQw-k';
		if ( $curl = curl_init() ) {
			curl_setopt( $curl, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace( ' ', "+", $address ) . '&key=' . $user_api_key );
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			$json_responce = json_decode( curl_exec( $curl ) );
			curl_close( $curl );

			if ( $json_responce->status == 'OK' ) {
				$lat_coord = $json_responce->results[0]->geometry->location->lat;
				$lng_coord = $json_responce->results[0]->geometry->location->lng;
			}
		}

		//check_admin_referer( 'my_action_date', 'my_date' );
		update_post_meta( $post_id, '_name', $name );
		update_post_meta( $post_id, '_desc', $desc );
		update_post_meta( $post_id, '_address', $address );
		update_post_meta( $post_id, '_lat', $lat_coord );
		update_post_meta( $post_id, '_lng', $lng_coord );
	}
}

/**
 *register and include my script & style
 */
add_action( 'wp_enqueue_scripts', 'add_my_scripts' );
function add_my_scripts() {
	$current_post_type = get_post_type();
	$meta_fields       = get_post_custom();
/**For default values*/
	$lat_coord       = ( empty( $meta_fields['_lat'] ) ) ? '' : (float) $meta_fields['_lat'][0];
	$lng_coord       = ( empty( $meta_fields['_lng'] ) ) ? '' : (float) $meta_fields['_lng'][0];
	$content_name    = ( empty( $meta_fields['_name'] ) ) ? '' : esc_html( $meta_fields['_name'][0] );
	$content_address = ( empty( $meta_fields['_address'] ) ) ? '' : esc_html( $meta_fields['_address'][0] );

	$content_string  = '<div id="content"><h3 id="firstHeading" class="firstHeading">' . $content_name . '</h3><p>' . $content_address . '</p></div>';

	if ( $current_post_type == 'mystores' && ! empty( $lat_coord ) && ! empty( $lng_coord ) && is_singular() ) {
		wp_enqueue_script( 'my-google-script', plugins_url( '/google-custom-script.js', __FILE__ ), array( 'google-maps' ) );
		wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD7PyCZw-qysy7M6O4GNmoY_ASSdRhQw-k', array( 'jquery' ) );
		wp_enqueue_style( 'my-google-script', plugins_url( '/my-style.css', __FILE__ ) );
		wp_localize_script( 'my-google-script', 'data_map_to_js', array(
			'lat_coord'      => $lat_coord,
			'lng_coord'      => $lng_coord,
			'content_string' => $content_string,
			'title'          => 'Store: ' . $content_name,
		) );
	}
}

/**
 * add map to mystore post_type
 */
add_filter( 'the_content', 'add_map_to_store' );
function add_map_to_store( $content ) {
	$current_post_type = get_post_type();
	if ( $current_post_type == 'mystores' && is_singular() ) {
		$content .= "<div id='my-google-map'></div>";
	}

	return $content;
}

/**
 * add shortcode
 */
add_shortcode( 'storemap', 'my_map_shortcode' );
function my_map_shortcode() {
	$args_storemap = array(
		'post_type'   => 'mystores',
		'post_status' => 'publish',
	);

	$posts_store = get_posts( $args_storemap );

	wp_enqueue_script( 'my-shortcode-script', plugins_url( '/shortcode-script.js', __FILE__ ), array( 'google-maps' ) );
	wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD7PyCZw-qysy7M6O4GNmoY_ASSdRhQw-k', array( 'jquery' ) );
	wp_enqueue_style( 'my-shortcode-script', plugins_url( '/my-style.css', __FILE__ ) );

	foreach ( $posts_store as $post ) {
		$data[] = array(
			'lat_coord'      => (float) $post->_lat,
			'lng_coord'      => (float) $post->_lng,
			'content_string' => '<div id="content"><h3 id="firstHeading" class="firstHeading">' . esc_html( $post->_name ) . '</h3><p>' . esc_html( $post->_address ) . '</p></div>',
		);
	}

	wp_localize_script( 'my-shortcode-script', 'data_map_shortcode_to_js', $data );

	return '<div id="my-shortcode-map"></div>';
}