/**
 * Created by User on 23.06.2017.
 */

jQuery(document).ready(function ($) {
    var uluru = {lat: Number(data_map_to_js.lat_coord), lng: Number(data_map_to_js.lng_coord)};

    var map = new google.maps.Map(document.getElementById('my-google-map'), {
        zoom: 16,
        center: uluru
    });

    var infowindow = new google.maps.InfoWindow({
        content: data_map_to_js.content_string
    });

    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        title: data_map_to_js.title
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
});

