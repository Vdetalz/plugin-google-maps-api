/**
 * Created by User on 26.06.2017.
 */
jQuery(document).ready(function ($) {
    var locations = data_map_shortcode_to_js;
    var map = new google.maps.Map(document.getElementById('my-shortcode-map'), {
        zoom: 10,
        center: new google.maps.LatLng(data_map_shortcode_to_js[0].lat_coord, data_map_shortcode_to_js[0].lng_coord),
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i].lat_coord, locations[i].lng_coord),
            map: map,
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i].content_string);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
});